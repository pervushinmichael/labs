let question_num = 0;

let questions_array = document.getElementsByClassName("question");

questions_array[question_num].style.display = "inline-block";

let getNext = ()=>{
	questions_array[question_num].style.display = "none";
	if(question_num < questions_array.length-1){
		question_num++;
	} else {
		question_num = 0;
	}
	questions_array[question_num].style.display = "inline-block";

}

let getPrev = ()=>{
	questions_array[question_num].style.display = "none";
	if(question_num ==0){
		question_num = questions_array.length-1;
	} else {
		question_num--;
	}
	questions_array[question_num].style.display = "inline-block";
}