const startForm = ()=>{
    let params = 
            "left=10,top=15,width=650,height=950,"+
            "menubar=no,loolbar=no,location=0,status=yes,"+
            "resizable=no,scrollbars=no";
            window.open("form.html","_blank", params);
}

const showTime = ()=>{
    let dateTime = new Date();
    document.getElementById("load-time").innerHTML += dateTime.toLocaleString();
    showCurrentTime();
}

const showCurrentTime = ()=>{
    let time = new Date();
    document.getElementById("current-time").innerHTML = formatter(time.getHours()) + ":" + formatter(time.getMinutes()) + ":" + formatter(time.getSeconds());
}

const formatter = (i) =>{
    if(i<10) return "0"+i;
    return i;
}

const showBrowserInfo = ()=>{
    let ua = navigator.userAgent;
    let bName  = "";
    if (ua.indexOf("Firefox") > -1)
        bName = "Mozilla Firefox";
    else if (ua.indexOf("Opera") > -1)
        bName= "Opera";
    else if (ua.indexOf("Trident") > -1)
        bName = "Microsoft Internet Explorer";
    else if (ua.indexOf("Edge") > -1)    
        bName = "Microsoft Edge";
    else if (ua.indexOf("Chrome") > -1)
        bName = "Google Chrome";
    else if (ua.indexOf("Safari") > -1)
        bName = "Apple Safari";
    else
        bName = "unknown";

    let bVersion = "";

   switch(bName){
        case "Mozilla Firefox" : bVersion = (ua.split("Firefox/")[1]).split(" ")[0]; break;
        case "Opera" : bVersion = (ua.split("Opera/")[1]).split(" ")[0]; break;
        case "Microsoft Internet Explorer" : bVersion = (ua.split("Trident/")[1]).split(" ")[0]; break;
        case "Microsoft Edge" : bVersion = (ua.split("Edge/")[1]).split(" ")[0]; break;
        case "Google Chrome" : bVersion = (ua.split("Chrome/")[1]).split(" ")[0]; break;
        case "Apple Safari" : bVersion = (ua.split("Safari/")[1]).split(" ")[0]; break;
    }
    document.getElementById("brwName").innerHTML +=bName;
    document.getElementById("brwVersion").innerHTML +=bVersion;
}


