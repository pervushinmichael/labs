const check = ()=>{

    let isOk = true;

    let surname_error = document.getElementById("surname-error");
    if(!document.getElementById("surname-user").value){
        surname_error.style.display = "inline-block";
        isOk = false;
    } else {
        surname_error.style.display = "none";
    }

    let name_error = document.getElementById("name-error");
    if(!document.getElementById("name-user").value){
        name_error.style.display = "inline-block";
        isOk = false;
    } else {
        name_error.style.display = "none";
    }

    let patronymic_error = document.getElementById("patronymic-error");
    if(!document.getElementById("patronymic-user").value){
        patronymic_error.style.display = "inline-block";
        isOk = false;
    } else {
        patronymic_error.style.display = "none";
    }

    let email = document.getElementById("email-user");
    let email_error = document.getElementById("email-error");
    if(!email.value){
        email_error.innerHTML = "Введите email"
        email_error.style.display = "inline-block";
        isOk = false;
    } else {
        if((email.value.split('@').length-1!=1)||( email.value.split('.').length-1 <= 0)){
            email_error.innerHTML = "Некорректный email"
            email_error.style.display = "inline-block";
            isOk = false;
        } else {
            email_error.style.display = "none";
        }
    }

    let pass = document.getElementById("password-user");
    let password_error = document.getElementById("password-error");
    if(!pass.value){
        password_error.innerHTML = "Введите пароль"
        password_error.style.display = "inline-block";
        isOk = false;
    } else {
       if(pass.value.length < 5){
           password_error.innerHTML = "короткий пароль";
           password_error.style.display = "inline-block";
           isOk = false;
       } else {
           password_error.style.display = "none";
       }
    }

    let arr_langs = document.getElementsByClassName("input-checkbox");
    for(let i = 0;i<arr_langs.length;i++){
        if(arr_langs[i].checked){
            document.getElementById("lang-error").style.display = "none";
            break;
        }
        if(i==arr_langs.length-1){
            isOk = false;
            document.getElementById("lang-error").style.display = "inline-block";
        }
    }

    let arr_jobs = document.getElementsByClassName("job-select");
    for(let i = 0;i<arr_jobs.length;i++){
        if(arr_jobs[i].selected){
            document.getElementById("job-error").style.display = "none";
            break;
        }
        if(i==arr_jobs.length-1){
            isOk = false;
            document.getElementById("job-error").style.display = "inline-block";
        }
    }
    return isOk;
}

const hide = ()=>{
    document.getElementById("job-error").style.display = "none";
    document.getElementById("lang-error").style.display = "none";
    document.getElementById("password-error").style.display = "none";
    document.getElementById("email-error").style.display = "none";
    document.getElementById("patronymic-error").style.display = "none";
    document.getElementById("name-error").style.display = "none";
    document.getElementById("surname-error").style.display = "none";
}