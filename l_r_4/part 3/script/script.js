$(function(){
	$("#window-one").draggable({
		containment: '.window-one-container', 
		revert:true,
	});
	$("#window-one").resizable({
		maxHeight:180,
		maxWidth:300,
		minHeight:150,
		minWidth:150
	});

	$(".task-two ul").sortable({axis:"y",containment:".task-two ul"});

	$("#window-two").draggable({
		containment: '.window-two-container',
		start: function(){
			$(this).css('z-index', 1);
		},
		stop: function(){
			$(this).css('z-index', 0);
		}
	});
	$("#window-three").draggable({
		containment: '.window-two-container'
	});
	$("#area-one").droppable({
		accept:"#window-two, #window-three",
		drop:function(ev,ui){
			$(ui.draggable).animate({
				left:"30px",
				top:"260px"
			},200,function(){
				$("#area-one").css("background-color","green");
			});
			$(ui.draggable).animate({
				opacity: "0"
			},200,function(){
				$(this).css("display","none");
			});
		}

	});
	$("#area-two").droppable({
		accept:"#window-three",
		drop:function(){
			$("#window-three").animate({
				left:"220px",
				top:"260px"
			},200,function(){
				$("#area-two").css("background-color","green");
			});
			$("#window-three").animate({
				opacity: "0"
			},200,function(){
				$(this).css("display","none");
			});
		}
	});
})