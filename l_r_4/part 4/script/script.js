$(function(){
	//получаем массив пазлов
	let puzzles = document.getElementsByClassName("puzzle");

	//перемешивание картинок
	let arrPuzzles = new Array(puzzles.length);
	for(let i=0;i<puzzles.length;i++){
		arrPuzzles[i] = puzzles[puzzles.length-i-1].id;
	}

	//массив areas
	let areas = document.getElementsByClassName("area");

	//массив для areaIDs
	let areasIDs = new Array(areas.length);
	{
		for(let i=0;i<areas.length;i++){
			areasIDs[i] = areas[i].id;
		}
	}

	//массив для проверки
	let arrArea = new Array(areas.length);
	for(let i=0;i<arrArea.length;i++) arrArea[i]=false;

	//вывод изображений
	for(let i=0;i<arrPuzzles.length;i++){
		
		$("#"+arrPuzzles[i]+" img").attr("src",("./res/"+(i+1)+".jpg"));
	}

	let currentPuzzleId;
	$(".puzzle").draggable({
		containment: '.wrapper',
		start:function(){
			currentPuzzleId = $(this).attr('id');
			arrArea[arrPuzzles.indexOf(currentPuzzleId)]=false;
		}
	});

	$(".area").droppable({
		accept:".puzzle",
		drop:function(){
			let offseX = $(this).css("left");
			let offsetY = $(this).css("top");
			$("#"+currentPuzzleId).animate({
				left : offseX,
				top : offsetY
			},200);
			let areaIndex = areasIDs.indexOf($(this).attr('id'));
			if(areaIndex == arrPuzzles.indexOf(currentPuzzleId)){
				arrArea[areaIndex] = true;
			}
			showResult();
		}
	});


	let check = ()=>{
		for(let i=0;i<arrArea.length;i++){
			if(arrArea[i]==false) return false;
		}
		return true;
	}

	let showResult = ()=>{
		if(!check()) return;
		$(".puzzle").draggable({
			cancel : ".puzzle"
		});
		$(".area").css("box-shadow","0 0 0 white");
		$(".puzzle").css("box-shadow","0 0 0 white");
		$("#invisible-block").toggleClass("shadowBlock");
		$("#invisible-block").css("z-index","5");
		$(".block").animate({
			width: "940px"
		},500)
		$(".header").animate({
			opacity: 0
		},500,function(){
			$(".header").html("Пазл собран");
		})
		$(".header").animate({
			opacity: 1
		},500)
	}
})