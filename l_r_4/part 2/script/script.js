$(function(){
	$('.block').css({
		"top":$(window).height()/2 - $('.block').height()/2,
		"left":$(window).width() - $('.block').width()
	})
	$("#start-button").css({
		"top":$(window).height()/2 - $('#start-button').height()/2,
		"left":$(window).width()/2 - $('#start-button').width()/2,
		"display":"block"
	})
	$(".block-green").css({
		"top":0,
		"left":$(window).width()/2 - $('.block-green').width()/2
	})
	$("#start-button").mouseover(function(){
		if($("#start-button").css("opacity")==1)
			$(this).stop(true);
		$(this).animate({
			borderTopWidth:'8px',
			borderLeftWidth:'8px',
			borderRightWidth:'8px',
			borderBottomWidth:'3px',
		},200);
	});
	$("#start-button").mouseout(function(){
		
		$(this).animate({
			borderTopWidth:'3px',
			borderLeftWidth:'3px',
			borderRightWidth:'3px',
			borderBottomWidth:'8px',
		},200);
	});
	$("#start-button").click(function(){
		$("#start-button").animate({
			left:'10px',
			opacity:0
		},200,function(){
			$("#start-button").css({"display":"none"});
			$(".block").animate({
				"left":$(window).width()/2 - $('.block').width()/2,
				"opacity":1
			},200);
			$(".block").animate({
				"top":$(window).height() - $('.block').height()
			},1000);
			$(".block").animate({
				"top":0
			},1000);
			$(".block").animate({
				"opacity":0
			},1000);
			$(".block").animate({
				"opacity":1
			},1000);
			$(".block").animate({
				backgroundColor:"#009624"
			},1000);
			$(".block").animate({
				backgroundColor:"black"
			},1000);
			$(".block").animate({
				"top":$(window).height()/2 - $('.block').height()/2
			},1000);
			$(".block").animate({
				"left":$(window).width() - $('.block').width(),
				"opacity":0
			},200,function(){
				$("#start-button").css({"display":"block"});
				$("#start-button").animate({
					left:$(window).width()/2 - $('#start-button').width()/2,
					"opacity":1
				},200)
			});
		})
	})
})