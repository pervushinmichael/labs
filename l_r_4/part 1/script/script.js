$(function(){
	let animPOne = true;
	$(".hide-btn").click(function(){
		$("#p-1").stop(true);
		$("#p-1").animate({opacity:0},1000);
	});

	$(".show-btn").click(function(){
		$("#p-1").stop(true);
		$("#p-1").animate({opacity:1},1000);
	});

	$(".button").mouseover(function(){
		$(this).stop(true);
		$(this).animate({
			borderTopWidth:'3px',
			borderLeftWidth:'3px',
			borderRightWidth:'3px',
			borderBottomWidth:'1px',
		},200);
	});

	$(".button").mouseout(function(){
		$(this).stop(true);
		$(this).animate({
			borderTopWidth:'1px',
			borderLeftWidth:'1px',
			borderRightWidth:'1px',
			borderBottomWidth:'3px',
		},200);
	});

	$("#p-2").mouseover(function(){
		$(this).stop(true);
		$(this).animate({
			opacity: 0
		},1000);
	});

	$("#p-2").mouseout(function(){
		$(this).stop(true);
		$(this).animate({
			opacity: 1
		},1000);
	});

	$(".compression-btn").click(function(){
		$("#rectangle").stop(true);
		$("#rectangle").animate({
			height:'0px',
			borderWidth:'0px'
		},1000);
	});

	$(".expansion-btn").click(function(){
		$("#rectangle").stop(true);
		$("#rectangle").animate({
			height:'150px',
			borderWidth:'2px'
		},1000);
	});

	let animBlock = true;
	$(".start-anim-btn").click(function(){
		$(".main-block").stop(true);
		$(".main-block").animate({
			left: '10px',
			top: '10px'
		},200);
		$(".main-block").animate({
			left: '150px',
			top: '50px'
		},1000);
		$(".main-block").animate({
			left: '300px',
			top: '150px'
		},1000);
		$(".main-block").animate({
			left: '500px',
			top: '300px'
		},1000);
		$(".main-block").animate({
			left: '150px',
			top: '300px'
		},1000);
		$(".main-block").animate({
			left: '50px',
			top: '150px'
		},1000);
		$(".main-block").animate({
			left: '10px',
			top: '10px'
		},1000);
	});
});