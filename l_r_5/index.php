<!DOCTYPE html>
<html>
<head>
	<title>Lab-five</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/style.css?<?echo time();?>">
	<link href="https://fonts.googleapis.com/css?family=Alegreya&display=swap" rel="stylesheet">
</head>
<body>
	<?php require "script/db.php"; ?>
	<div class="wrapper">
		<div class="content">
			<div class="content-inner">
				<div class="groups">
					<div class="groups-inner">
						<?php 
							$groups = getGoodsGroups();
							foreach($groups as $group):
						?>
						<div class="group">
							<div class="group-a-active">
								<a href= <?php echo "index.php?id_group=".$group["id_group"];?>>
									<?php echo $group["name_group"];?>
								</a>
							</div>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="goods">
					<div class="goods-inner">
						<?php 
							$goods = getGoods();
							foreach($goods as $good):
						?>
						<div class="good">
							<div class="good-inner">
								<div class="good-name">
									<?php echo $good["name_good"];?>
								</div>
								<div class="good-params">
									<div class="good-params-inner">
										<div class="good-quantity">
											<?php echo "На складе: ".$good["quantity"]." шт.";?>
										</div>
										<div class="good-price">
											<?php echo $good["price"]." ₽";?>
										</div>
										<div class="good-addBasket">
											<div class="good-addBasket-active">
												<a href= <?php 
													if($good["quantity"]>0){
														echo "index.php?id_good=".$good["id_good"]."&id_group=".getGroupId($good["id_good"]);
													}
													else
														echo "# style= display:none;";
												?>>
													В корзину
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="basket">
			<div class="basket-inner">
				<div class="basket-header">
					Корзина
				</div>
				<div class="basket-empty">Добавте элементы</div>
				<?php 
					$basket = getBasket();
					foreach ($basket as $bItem):
				?>
				<div class="basket-item">
					<div class="basket-item-inner">
						<div class="basket-item-name"><?php echo $bItem["name_good"]; ?></div>
						<div class="basket-item-params">
							<div class="basket-item-params-inner">
								<div class="basket-item-price"><?php echo $bItem["price"]." ₽"; ?></div>
								<div class="remove-basket-item">
									<div class="remove-basket-item-active">
										<a href=<?php echo "index.php?order_id=".$bItem["order_id"]."&id_group=".getGroupId($good["id_good"]); ?>>
											Удалить
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</body>
</html>