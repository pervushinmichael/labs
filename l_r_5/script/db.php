<?php
	session_start();
	if(empty($_SESSION[session_id()])) $_SESSION[session_id()] = time();
	$pdo = new PDO('mysql:host=localhost; dbname=lab_five','root','toor');

	function getGoodsGroups(){
		global $pdo;
		$groups = $pdo->query("SELECT * FROM goods_groups");
		return $groups;
	}
	function getGoods(){
		global $pdo;
		if(isset($_GET["id_group"])) $id = $_GET["id_group"];
		else $id = ($pdo->query("SELECT id_group FROM goods_groups"))->fetch(PDO::FETCH_ASSOC)["id_group"];
		$goods = $pdo->query("SELECT * FROM goods WHERE id_group = $id");
		return $goods;
	}

	function getQuantity($goodID){
		global $pdo;
		$quantity = ($pdo->query("SELECT quantity FROM goods WHERE id_good = $goodID"))->fetch(PDO::FETCH_ASSOC)["quantity"];
		return $quantity;
	}

	function getGroupId($goodID){
		global $pdo;
		$groupID = ($pdo->query("SELECT id_group FROM goods WHERE id_good = $goodID"))->fetch(PDO::FETCH_ASSOC)["id_group"];
		return $groupID;
	}

	function getBasket(){
		$sessionId = $_SESSION[session_id()];
		global $pdo;
		$basket = $pdo->query("SELECT order_id,name_good,price FROM orders JOIN goods USING(id_good) WHERE session_id = $sessionId");
		return $basket;
	}

	if(isset($_GET["id_good"])){
		$id_good = $_GET["id_good"];
		if(getQuantity($id_good)>0){
			$sessionId = $_SESSION[session_id()];
			$orderCheck = $pdo->query("SELECT COUNT(*) AS orderCheck FROM orders WHERE id_good = $id_good and session_id = $sessionId")->fetch(PDO::FETCH_ASSOC)["orderCheck"];
			if($orderCheck==0){
				$pdo->query("INSERT INTO orders (session_id,quantity,id_good) VALUES ($sessionId,1,$id_good)");
				$pdo->query("commit");
			}
		}
	}

	if(isset($_GET["order_id"])){
		$order_id = $_GET["order_id"];
		$sessionId = $_SESSION[session_id()];
		$pdo->query("DELETE FROM orders WHERE order_id = $order_id AND session_id = $sessionId");
	}
?>